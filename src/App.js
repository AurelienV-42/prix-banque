import React from 'react';
import './App.css';
import './i18n';

import firebase from 'firebase/app';// https://firebase.google.com/docs/firestore
import 'firebase/firestore';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom'; // https://reactrouter.com/web/guides/quick-start
import {useTranslation} from 'react-i18next';

import TransferScreen from './screens/transfers/TransferScreen';
import Home from './screens/home/Home';

const App = () => {
  const {t, i18n} = useTranslation();
  const db = firebase.firestore();

  db
    .doc('settings/version')
    .get()
    .then((value) => {
      console.log('Version number in firebase:', value.data().number);
    });

  // db
  //   .collection(`settings`)
  //   .doc('version')
  //   .get()
  //   .then((value) => {
  //     console.log('Firestore:', value.data());
  //   });

  // db
  //   .collection(`users/${global.uid}/conversations/`)
  //   .orderBy('date', 'asc')
  //   .onSnapshot((value) => {
  //     console.log('Firestore:', value.data());
  //   });

  // db
  //   .doc(`users/${getGlobal().uid}/conversations/bob`)
  //   .set({});

  // db
  //   .collection(`users/${getGlobal().uid}/conversations/bob/messages`)
  //   .add(newMessage);

  // db
  //   .doc(`users/${getGlobal().uid}`)
  //   .update({
  //       credits: getGlobal().user.credits + 1,
  //       creditsUsed: getGlobal().user.creditsUsed + 1,
  //     });

  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">{t('home')}</Link>
            </li>
            <li>
              <Link to="/transfers">{t('transfers')}</Link>
            </li>

            <li><a onClick={() => {
              i18n.changeLanguage(i18n.language === 'fr' ? 'en' : 'fr');
            }} className="smoothscroll" href="#home">{t(i18n.language === 'fr' ? 'english' : 'french')}</a></li>
          </ul>
        </nav>

        <Switch>
          <Route path="/transfers">
            <TransferScreen />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
