import {useTranslation} from 'react-i18next';

const TransferScreen = () => {
  const {t} = useTranslation();

  return <h2>{t('transfers')}</h2>;
};

export default TransferScreen;
