import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import './config';

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
